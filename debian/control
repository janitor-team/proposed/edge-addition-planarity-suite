Source: edge-addition-planarity-suite
Maintainer: Debian Science Maintainers <debian-science-maintainers@alioth-lists.debian.net>
Uploaders: Julien Puydt <jpuydt@debian.org>
Section: math
Priority: optional
Standards-Version: 4.5.0
Homepage: https://github.com/graph-algorithms/edge-addition-planarity-suite
Build-Depends: debhelper-compat (= 13)
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/science-team/edge-addition-planarity-suite.git
Vcs-Browser: https://salsa.debian.org/science-team/edge-addition-planarity-suite

Package: libplanarity0
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Library of planarity-related graph algorithms
 This library contains the reference implementation of the
 Edge Addition Planarity Algorithm, which is the best
 linear-time method to embed a planar graph and isolate
 planarity obstructions.
 .
 This package contains the library.

Package: libplanarity-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}, libplanarity0 (= ${binary:Version})
Description: Library of planarity-related graph algorithms (devel files)
 This library contains the reference implementation of the
 Edge Addition Planarity Algorithm, which is the best
 linear-time method to embed a planar graph and isolate
 planarity obstructions.
 .
 This package contains the development files.

Package: planarity
Architecture: any
Depends: libplanarity0 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: Program for planarity-related graph algorithms
 This package contains a command-line reference implementation of the
 Edge Addition Planarity Algorithm, which is the best linear-time
 method to embed a planar graph and isolate planarity obstructions.
